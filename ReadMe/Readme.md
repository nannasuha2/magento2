# Magento Tutorial

## Magento Setup

1. Install xampp in C:/xampp. Make sure to:
   * disable anti-virus.
   * Install all packages in xampp.
   * change port for Skype or Uninstall the Skype.
   * Use Netstat to check used ports.
   * Don't ever choose to install latest xampp. 
2. Create repository for magento backup. Please refer to Readme.md in D:/Repositories.
3. Download Magento Software.
   * Go to https://magento.com/tech-resources/download
   * Choose either **magento package with sample** (better) or **without sample**.
   * Put in C:\xampp\htdocs\magento

## Create Configuration

Creating custom extensions and modules.

Extension is referred to a brand/company name.

Modules is referred to attribute for the brand. Eg. of modules are widget, menu and etc.

1. Open C:\xampp\htdocs\magento\app

2. create folder "code"

3. In "code" folder put the custom module named as "HelloWorld".

4. Open "HelloWorld" and create extension folder, "etc" folder to put module extension setting in .xml format. Create "module.xml" as below:

   ```xml
   <?xml version="1.0"?>
   <config xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../../../../../lib/internal/Magento/Framework/Module/etc/module.xsd">
       <module name="Amasty_HelloWorld" setup_version="0.0.0">
       </module>
   </config>
   ```

   > #### Important Step!!!
   >
   > Each time new file added, run these in **command line**:
   >
   > * php bin/magento setup:upgrade
   > * php bin/magento cache:clean
   > * php bin/magento indexer:reindex
   >
   > Open command line in folder where magento is installed.

5. Then ..

## How to Configure the Disable Module Output

No longer can configure through magento/admin page. 

Go to magento>>app>>etc>>config.php

![image of config.php](C:\Users\NASUHA\AppData\Roaming\Typora\typora-user-images\1540118188825.png)



![image of magento page admin.](C:\Users\NASUHA\AppData\Roaming\Typora\typora-user-images\1540158026022.png)

There is in page admin. store>> setting>> configuration >> advanced >> advanced.

## How to configure default page in Magento

## Configured Front-end Output



## Configured unloaded logo and images in the page

1. Go to app/etc/di.xml
2. Change "Symlink" to "Copy" in line 607



------

# Magento Tutorial Conference (Magento2 Folder)

* How to setup an online store?
* How to customize Magento 2?



##### PathInfoProcessor.php

![1540427378925](C:\xampp\htdocs\magento2\1540427378925.png)

_*aroundDispatch*_

-for front controller



# Start Creating Module

**Software IDE: PHP Storm**

1. Set **< code >** as the **root**. 

   * Right-click **< code >** >>  Mark directory as >>Sources Root
   * It will auto complete _**namespace**_ which provide the class by the directory structure.

2. Inside **< code >**, right-click to create a directory. The directory is _**InteractOne/GeoIpRedirect/etc/**_

   * Every module will need an **< etc >**, excentra
   * **InteractOne**: Extension
   * **GeolpRedirect**: Module
   * Module Name: **InteractOne_GeolpRedirect**

3. Create a **module.xml** inside **GeolpRedirect**

   * right-click >> create new >> type in M2 >> M2 Module XML (actually template) -saving commonly used code as template file.

   * File name: module, Module Name: InteractOne_GeolpRedirect

     > #### To Save File as Template
     >
     > tool >> save file as a template
     >
     > can specify changeable variable. Eg:
     >
     > ```xml
     > <module name= "InteractOne_GeolpRedirect" setup_version= "2.0.0"/>
     > ```
     >
     >  into
     >
     > ```xml
     > <module name= "$(MODULE_NAME)" setup_version= "$(SETUP_VERSION)"/>
     > ```

4. Create a "**registration.php**" in the **GeolpRedirect** (the root folder)

   * Using the template also
   * To let the module being known as a module

5. Create a "**composer.json**"

   * Required to include dependencies. (**But it is optional)

   * Make a template also with these attributes:

     * File name: composer
     * EXTENSION: InteractOne
     * MODULE: GeolpRedirect
     * DESCRIPTION: N/A
     * NAMESPACE: interactone

   * Refer image below and do modification accordingly,

     ![1540435517947](C:\xampp\htdocs\magento2\1540435517947.png)

6. Create **< Setup >** inside **GeolpRedirect**

   * For data sequence

7. Create Php class, "**UpgradeSchema.php**"

   * auto-generated: namespace, class name, file name. ~ because has mark Extension file as root file.

   * Help update for any changes made. (if we have different module version, data or table)

     ```php
     <?php 
     namespace InteractOne\GeolpRedirect\Setup;/* auto-generate*/
     
     use Magento\Framework\Setup\UpgradeSchemaInterface; /*auto-generate after typing implements UpgradeSchemaInterface*/
     
     /*class UpgradeSchema is auto-generate*/
     class UpgradeSchema implements UpgradeSchemaInterface
     {
         
     }
         
         ?>
     ```

   * Implement "**UpgradeSchemaInterface**" as its interface.

   * this code not complete yet and produce error. Click Alt + Enter to auto completion.

     ```php
     public function upgrade(SchemaSetupInterface $setup, ModuleContexInterface $context)
     {
         $contex->getVersion(); /*this to get version. Not used in the excercise*/
         
         $setup->startSetup();
         
         $setup->getConnection()->addColumn(
             $setup->getTable('store'),'country_code',
             [
                 'type'=> Table::TYPE_TEXT,
                 'length'=> 2,
                 'nullable'=> true,
                 'default'=> null,
                 'comment'=> 'Country Code'
             ]
         )
         
         $setup->endSetup();
     }
     ```

8. Create **< frontend >** inside **< etc >**

9. Inside **< frontend >**, create "**di.xml**"

   * to hook into Magento frontend interface.

   * to specify the scope where the code is being applied.

     ```xml
     <?xml verssion="1.0"?>
     
     <config 
       xmlns:xsi="http://www.w3.org/2001/xmlSchema-instance"
       xsi:noNamespaceSchemaLocation="urn:magento:framework:ObjectManager/etc/config">
     	<type name="\Magento\Framework\App\FrontControllerInterface">
             <plugin name="GeolpRedirect" type="InteractObe\GeilpRedirect\Plugin\GeolpRedirectPlugin"/>
         </type>
     
     </config>
             
            
     ```

10. Create **< Plugin >** inside the module.

    * it make easier whenever to configure the module.

11. Inside **< Plugin >**, create php class:-

    * Name: GeolpRedirectPlugin

    *  Namespace: InteractObe\GeilpRedirect\Plugin

    * File name: GeolpRedirectPlugin

      ```php
      <?php
          
          namespace ...;
      	
      	class GeolpRedirectPlugin
          {
              
          }
          
          ?>
      ```

    * We have 3 types of plugin in magento2:

      * before 

        -call before the method, allow to change argument to the method.

      * after

        -called after the magento method being called, allow to change the return type.

      * around

        -called before, after and also provide instance to called it self.

      ```php
      class GeoIpRedirectPlugin
      {
          public function _construct() //using construct method. Object manager Dependencies injection -version control
          {
              
          }
          
          
          public function beforeDispatch(
          FrontControllerInterface $subject, RequestInterface $request)	{
              $countryCode = '';
              $store = '';
              //set store cookies
          } 
      }
      ```

12. 





# Trouble Shoot: There has been an error processing your request

Check files. There must be syntax error.



# HTML vs XML

**HTML** display data with focus on how data look

**XML** independent tool used to transport and store data, with focus on what data is.



![1540743366459](C:\xampp\htdocs\magento2\1540743366459.png)

![1540743626034](C:\xampp\htdocs\magento2\1540743626034.png)

![1540743569567](C:\xampp\htdocs\magento2\1540743569567.png)

![1540743592262](C:\xampp\htdocs\magento2\1540743592262.png)











